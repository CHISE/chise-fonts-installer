#!/bin/sh

PATH=/usr/local/share/scripts:/usr/local/bin:/sw/bin:/usr/X11R6/bin:/usr/bin:/bin:/sw/sbin:/usr/sbin:/sbin:$PATH
export PATH

[ -f /usr/local/share/scripts/chise-setup-xfonts ] && . chise-setup-xfonts
[ -f /usr/local/share/scripts/misc-setup-xfonts ] && . misc-setup-xfonts
[ -f /usr/local/share/scripts/local-setup-xfonts ] && . local-setup-xfonts
