2019-05-10  MORIOKA Tomohiko  <tomo.git@chise.org>

	* BDF/Makefile.in (distclean-all): Add cleaner for bdf-hng-fonts.

2018-10-14  MORIOKA Tomohiko  <tomo.git@chise.org>

	* TrueType/Hanazono/Makefile.in (HANAZONO_FONTS_URL): Modify URL.
	(HANAZONO_ZIP_FILE): Update to hanazono-20170904.zip.

2018-10-01  MORIOKA Tomohiko  <tomo.git@chise.org>

	* BDF/Makefile.in (install): Depend on `install-HNG'.
	(build-HNG): Depend on `HNG'.

2018-09-23  MORIOKA Tomohiko  <tomo.git@chise.org>

	* chise-setup-xfonts.in: Add @prefix@/share/fonts/PCF/HNG.

	* BDF/Makefile.in (all): Add `build-HNG'.
	(HNG): New target.
	(build-HNG): New target.
	(install-HNG): New target.
	(distclean): Add cleaner for HNG/.

2018-09-17  MORIOKA Tomohiko  <tomo.git@chise.org>

	* Makefile.in (install-mac): New target.
	(install-truetype-mac): New target.
	(install-scripts-mac): New target.

	* site-setup-xfonts.sh: New file. [copied from kanbun/env/]

2014-12-09  MORIOKA Tomohiko  <tomo.git@chise.org>

	* BDF/JISX0213/Makefile.in (JISX0213_16DOT_FONTS_URL): Use
	http://www.chise.org/dist/fonts/BDF/JISX0213/ instead of
	http://www12.ocn.ne.jp/~imamura.

2014-09-22  MORIOKA Tomohiko  <tomo.git@chise.org>

	* BDF/Hanazono/Makefile.in (BDF_FILES): Add
	HanaMin-UCS-SIP_20.bdf, HanaMin-UCS-SIP_40.bdf,
	HanaMin-UCS-SIP_48.bdf and HanaMin-UCS-SIP_50.bdf.
	(HanaMin-UCS-SIP_20.bdf): New target.
	(HanaMin-UCS-SIP_40.bdf): New target.
	(HanaMin-UCS-SIP_48.bdf): New target.
	(HanaMin-UCS-SIP_50.bdf): New target.

2014-04-28  MORIOKA Tomohiko  <tomo.git@chise.org>

	* configure.in: Generate BDF/Hanazono/Makefile.

	* BDF/Makefile.in (all): Add `build-hanazono'.
	(install): Add `install-hanazono'.
	(build-hanazono): New target.
	(install-hanazono): New target.
	(distclean): Add cleaner for Hanazono/.
	(distclean-all): Likewise.

2013-06-11  MORIOKA Tomohiko  <tomo.git@chise.org>

	* chise-setup-xfonts.in: Add
	@prefix@/share/fonts/PCF/Hanazono:unscaled and
	@prefix@/share/fonts/PCF/Hanazono.

	* BDF/Hanazono/Makefile.in: New file.

2012-07-23  MORIOKA Tomohiko  <tomo.git@chise.org>

	* Makefile.in (distclean): Delete GT/.

2012-07-23  MORIOKA Tomohiko  <tomo.git@chise.org>

	* chise-setup-xfonts.in: Delete
	@prefix@/share/fonts/BDF/JISX0213:unscaled.

	* BDF/JISX0213/Makefile.in (install):
	- Use "gzip -d <" instead of "zcat".
	- Don't run `mkfontdir' in JISX0213_BDF_FONTS_DIR.

2012-07-23  MORIOKA Tomohiko  <tomo.git@chise.org>

	* TrueType/Makefile.in (install-mac): New target.
	(install-hanazono-mac): Ditto.
	(install-gt-mac): Ditto.

	* TrueType/GT/Makefile.in (install-mac): New target.
	(/Library/Fonts/gt200001.ttf): Ditto.
	(/Library/Fonts/gt200002.ttf): Ditto.
	(/Library/Fonts/gt200003.ttf): Ditto.
	(/Library/Fonts/gt200004.ttf): Ditto.
	(/Library/Fonts/gt200005.ttf): Ditto.
	(/Library/Fonts/gt200006.ttf): Ditto.
	(/Library/Fonts/gt200007.ttf): Ditto.
	(/Library/Fonts/gt200008.ttf): Ditto.
	(/Library/Fonts/gt200009.ttf): Ditto.
	(/Library/Fonts/gt200010.ttf): Ditto.
	(/Library/Fonts/gt200011.ttf): Ditto.
	(/Library/Fonts/gt200012.ttf): Ditto.
	(/Library/Fonts/gt200013.ttf): Ditto.

2012-07-23  MORIOKA Tomohiko  <tomo.git@chise.org>

	* configure.in, Makefile.in, TrueType/Makefile.in: Move GT/ into
	TrueType/.

	* TrueType/GT/Makefile.in: Moved from GT/Makefile.in.

	* TrueType/GT/encodings.dir: Moved from GT/encodings.dir.

	* TrueType/GT/fonts.dir: Moved from GT/fonts.dir.

2012-07-19  MORIOKA Tomohiko  <tomo.git@chise.org>

	* chise-setup-xfonts.in: Add
	@prefix@/share/fonts/BDF/JISX0213:unscaled as a fallback font-path
	when `mkfontdir' fails in JISX0213_PCF_FONTS_DIR.

	* BDF/JISX0213/Makefile.in (install): Run `mkfontdir' in
	JISX0213_BDF_FONTS_DIR as a fallback when `mkfontdir' fails in
	JISX0213_PCF_FONTS_DIR.

2012-04-27  MORIOKA Tomohiko  <tomo.git@chise.org>

	* TrueType/Hanazono/Makefile.in (HANAZONO_FONTS_URL): Modify for
	hanazono-20120421.zip.
	(HANAZONO_ZIP_FILE): Likewise.
	($(HANAZONO_ZIP_FILE)): Specify -O $(HANAZONO_ZIP_FILE).

2012-03-14  MORIOKA Tomohiko  <tomo@zinbun.kyoto-u.ac.jp>

	* TrueType/Hanazono/Makefile.in (HANAZONO_ZIP_FILE): Update to
	"hanazono-20120202.zip".
	(get): Add `HanaMinB.ttf'.
	(HanaMinA.ttf): Depended on $(HANAZONO_ZIP_FILE).
	(HanaMinB.ttf): New target.
	($(HANAZONO_ZIP_FILE)): New target.

2011-09-19  MORIOKA Tomohiko  <tomo@zinbun.kyoto-u.ac.jp>

	* TrueType/Hanazono/Makefile.in: (HANAZONO_ZIP_FILE) Use
	hanazono-20110915.zip instead of hanazono-20110516.zip.

2011-08-31  MORIOKA Tomohiko  <tomo@zinbun.kyoto-u.ac.jp>

	* TrueType/Hanazono/Makefile.in (HANAZONO_FONTS_URL): Use
	http://kage.sourceforge.jp/hanazono instead of
	http://www.fonts.jp/hanazono.

2011-06-02  MORIOKA Tomohiko  <tomo@zinbun.kyoto-u.ac.jp>

	* TrueType/Hanazono/Makefile.in (install): Depend on `all'.
	(install-mac): New target.
	(/Library/Fonts/HanaMinA.ttf): New target.
	(/Library/Fonts/HanaMinB.ttf): New target.

2011-06-02  MORIOKA Tomohiko  <tomo@zinbun.kyoto-u.ac.jp>

	* BDF/JISX0213/Makefile.in (jiskan24-2003-1.bdf.gz): Use
	http://www.chise.org/dist/fonts/BDF/JISX0213/jiskan24-2003-1.bdf.gz.

	* TrueType/Makefile.in (all): Depend on `build-hanazono'.
	(install): Depend on `install-hanazono'.
	(build-hanazono): New target.
	(install-hanazono): New target.
	(distclean): Run "make distclean" in Hanazono/.
	(distclean-all): Run "make distclean-all" in Hanazono/.

	* configure.in: Generate TrueType/Hanazono/Makefile.

	* TrueType/Hanazono/Makefile.in, TrueType/Hanazono/fonts.alias,
	TrueType/Hanazono/fonts.dir: New files.

2011-04-28  MORIOKA Tomohiko  <tomo@zinbun.kyoto-u.ac.jp>

	* chise-setup-xfonts.in: Add Hanazono's path if it is found.

2011-05-20  MORIOKA Tomohiko  <tomo@zinbun.kyoto-u.ac.jp>

	* BDF/JISX0213/Makefile.in (jiskan24-2003-1.bdf.gz): Use
	http://www.chise.org/chise/dist/fonts/BDF/JISX0213/jiskan24-2003-1.bdf.gz.

2011-03-17  MORIOKA Tomohiko  <tomo@zinbun.kyoto-u.ac.jp>

	* BDF/JISX0213/Makefile.in (JISX0213_24DOT_FONTS_URL): Use
	http://distfiles.gentoo.org/distfiles instead of
	http://freebsd.yz.yamagata-u.ac.jp/pub/FreeBSD/ports/distfiles.
	(jiskan24-2003-1.bdf.gz): Use
	"http://s390.koji.fedoraproject.org/koji/fileinfo?rpmID=2670&filename=jiskan24-2003-1.bdf.gz"
	instead of
	http://cvs.fedoraproject.org/repo/pkgs/fonts-japanese/jiskan24-2003-1.bdf.gz/d452c1138a4684f864b8d12ec0e2f00d/jiskan24-2003-1.bdf.gz.

2010-11-22  MORIOKA Tomohiko  <tomo@kanji.zinbun.kyoto-u.ac.jp>

	* BDF/JISX0213/Makefile.in (JISX0213_24DOT_FONTS_URL): Use
	http://freebsd.yz.yamagata-u.ac.jp/pub/FreeBSD/ports/distfiles
	instead of http://gitatsu.hp.infoseek.co.jp/bdf.
	(jiskan24-2003-1.bdf.gz): Use
	http://cvs.fedoraproject.org/repo/pkgs/fonts-japanese/jiskan24-2003-1.bdf.gz/d452c1138a4684f864b8d12ec0e2f00d/jiskan24-2003-1.bdf.gz.

2010-09-17  MORIOKA Tomohiko  <tomo@kanji.zinbun.kyoto-u.ac.jp>

	* TrueType/Arphic/Makefile.in (get): Don't add .gz.

2010-09-17  MORIOKA Tomohiko  <tomo@kanji.zinbun.kyoto-u.ac.jp>

	* TrueType/Arphic/Makefile.in (install): Install encodings.dir.

2010-09-17  MORIOKA Tomohiko  <tomo@kanji.zinbun.kyoto-u.ac.jp>

	* configure.in: Generate TrueType/Arphic/Makefile.

	* chise-setup-xfonts.in: Add Arphic's path if it is found.

	* TrueType/Makefile.in (build-arphic): New target.
	(install-arphic): New target.
	(distclean): Run "make distclean" in Arphic/.
	(distclean-all): Run "make distclean-all" in Arphic/.

	* TrueType/Arphic/Makefile.in, TrueType/Arphic/encodings.dir,
	TrueType/Arphic/fonts.dir: New files.

2010-09-17  MORIOKA Tomohiko  <tomo@kanji.zinbun.kyoto-u.ac.jp>

	* Makefile.in (distclean): Don't delete font distribution files.
	(distclean-all): New target to delete font distribution files.

	* GT/Makefile.in (distclean): Don't delete fonts.
	(distclean-all): New target to delete fonts.

	* TrueType/Makefile.in (distclean-all): New target to delete
	fonts.

	* TrueType/Zinbun/Makefile.in (distclean): Don't delete fonts.
	(distclean-all): New target to delete fonts.

	* BDF/Makefile.in (distclean-all): New target to delete fonts.

	* BDF/Zinbun/Makefile.in, BDF/Thai-XTIS/Makefile.in,
	BDF/JISX0213/Makefile.in, BDF/ETL-VN2/Makefile.in,
	BDF/EGB/Makefile.in, BDF/CBETA/Makefile.in (distclean): Don't
	delete fonts.
	(distclean-all): New target to delete fonts.


2010-06-16  MORIOKA Tomohiko  <tomo@kanji.zinbun.kyoto-u.ac.jp>

	* chise-fonts-installer 0.2 released.

	* BDF/Zinbun/Makefile.in, BDF/Thai-XTIS/Makefile.in,
	BDF/JISX0213/Makefile.in, BDF/ETL-VN2/Makefile.in,
	BDF/EGB/Makefile.in, BDF/CBETA/Makefile.in (install):
	Use `gzip - 9' instead of `gzip -best'.


2010-06-16  MORIOKA Tomohiko  <tomo@kanji.zinbun.kyoto-u.ac.jp>

	* chise-fonts-installer 0.1 released.

	* Makefile.in (PACKAGE): New variable.
	(tar): New target.

	* configure.in: Rename `chise-fonts' to `chise-fonts-installer'.

2010-04-21  MORIOKA Tomohiko  <tomo@kanji.zinbun.kyoto-u.ac.jp>

	* BDF/EGB/Makefile.in: New file.

	* BDF/EGB/fonts.alias: New file.

	* BDF/Makefile.in (all): Call `build-egb'.
	(install): Call `install-egb'.
	(build-egb): New target.
	(install-egb): New target.
	(distclean): Run `make distclean' in EGB/.

	* chise-setup-xfonts.in: Add
	@prefix@/share/fonts/PCF/EGB:unscaled.

	* configure.in: Generate BDF/EGB/Makefile.

2009-12-18  MORIOKA Tomohiko  <tomo@kanji.zinbun.kyoto-u.ac.jp>

	* BDF/Makefile.in (distclean): Run in JISX0213/.

2009-12-17  MORIOKA Tomohiko  <tomo@kanji.zinbun.kyoto-u.ac.jp>

	* BDF/JISX0213/Makefile.in (JISX0213_16DOT_FONTS_URL): Renamed
	from `JISX0213_FONTS_URL'.
	(JISX0213_24DOT_FONTS_URL): New variable.
	(BDF_FILES): Add jiskan24-2000-1.bdf.gz, jiskan24-2000-2.bdf.gz
	and jiskan24-2003-1.bdf.gz.
	(jiskan24-2000-1.bdf.gz): New target.
	(jiskan24-2000-2.bdf.gz): New target.
	(jiskan24-2003-1.bdf.gz): New target.

2009-12-11  MORIOKA Tomohiko  <tomo@kanji.zinbun.kyoto-u.ac.jp>

	* BDF/JISX0213/Makefile.in: New file.

	* chise-setup-xfonts.in: Add
	@prefix@/share/fonts/PCF/JISX0213:unscaled.

	* BDF/Makefile.in (all): Call `build-jisx0213'.
	(install): Call `install-jisx0213'.
	(build-jisx0213): New target.
	(install-jisx0213): New target.

	* configure.in: Generate BDF/JISX0213/Makefile.

2009-12-11  MORIOKA Tomohiko  <tomo@kanji.zinbun.kyoto-u.ac.jp>

	* configure.in: Generate `chise-setup-xfonts'.

	* Makefile.in (MKDIR): New variable.
	(datadir): New variable.
	(SCRIPTS_DIR): New variable.
	(install): Call `all' and `install-scripts'.
	(install-scripts): New target.
	(distclean): Delete `chise-setup-xfonts'.

	* chise-setup-xfonts.in: New file.

2009-12-11  MORIOKA Tomohiko  <tomo@kanji.zinbun.kyoto-u.ac.jp>

	* Makefile.in (all): Call `build-bdf' and `build-truetype'.
	(install): Call `install-bdf' and `install-truetype'.
	(build-bdf): New target.
	(install-bdf): New target.
	(build-truetype): New target.
	(install-truetype): New target.
	(distclean): Run `make distclean' in BDF and TrueType.

	* configure.in: Generate BDF/Makefile, BDF/ETL-VN2/Makefile,
	BDF/Thai-XTIS/Makefile, BDF/Zinbun/Makefile, BDF/CBETA/Makefile,
	TrueType/Makefile and TrueType/Zinbun/Makefile.

	* TrueType/Makefile.in, TrueType/Zinbun/Makefile.in,
	BDF/Makefile.in, BDF/CBETA/Makefile.in, BDF/Thai-XTIS/Makefile.in,
	BDF/ETL-VN2/Makefile.in, BDF/Zinbun/Makefile.in: New files.

2009-12-07  MORIOKA Tomohiko  <tomo@kanji.zinbun.kyoto-u.ac.jp>

	* Makefile.in (distclean): Delete intlfonts-1.2.1/ and
	intlfonts-1.2.1.tar.gz.

2009-12-07  MORIOKA Tomohiko  <tomo@kanji.zinbun.kyoto-u.ac.jp>

	* Makefile.in (distclean): New target.

2009-12-06  MORIOKA Tomohiko  <tomo@kanji.zinbun.kyoto-u.ac.jp>

	* GT/Makefile.in: New file.

2003-10-27  MORIOKA Tomohiko  <tomo@kanji.zinbun.kyoto-u.ac.jp>

	* GT/encodings.dir: New file.

2002-12-25  MORIOKA Tomohiko  <tomo@kanji.zinbun.kyoto-u.ac.jp>

	* GT/fonts.dir: New file.

2009-12-06  MORIOKA Tomohiko  <tomo@kanji.zinbun.kyoto-u.ac.jp>

	* configure, config.guess, config.sub, install-sh: New files.

	* Makefile.in, configure.in: New files.

